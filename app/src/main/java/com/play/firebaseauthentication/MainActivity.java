package com.play.firebaseauthentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import Model.User;

public class MainActivity extends AppCompatActivity {

    private TextInputLayout emailLayout ;
    private TextInputLayout passwordLayout;


    String email;
    String password;

    Button signUp;
    private FirebaseDatabase firebaseDatabase;

    private FirebaseAuth firebaseAuth;

    private static final String TAG  = "MainActivity.java";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



         emailLayout =  findViewById(R.id.email_layout);
        signUp  = (Button)findViewById(R.id.signUpBtn);

         passwordLayout = findViewById(R.id.password_layout);


        TextView signUpText = (TextView)findViewById(R.id.signUpText);

        signUpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,RegisterActivity.class));
            }
        });




        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();



        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                email  = Objects.requireNonNull(emailLayout.getEditText()).getText().toString();

                password = Objects.requireNonNull(passwordLayout.getEditText()).getText().toString();


                firebaseAuth.signInWithEmailAndPassword(email,password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        
                        if(task.isSuccessful()){
                            Toast.makeText(MainActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                            FirebaseUser u = firebaseAuth.getCurrentUser();


                            assert u != null;
                            DatabaseReference ref = firebaseDatabase.getReference("users/" + u.getUid());

                            ref.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {

//                                    for(DataSnapshot d : snapshot.getChildren()){
//
//                                        String s = d.getValue(String.class);
//                                        Log.d("ondatachange", s);
//                                    }

                                    User user =  snapshot.getValue(User.class);

                                    assert user != null;
                                    Log.d(TAG, "onDataChange: "+user.getEmail());


                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {
                                    Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

                                }
                            });


                            Intent intent  = new Intent(MainActivity.this, HomePage.class);

                            intent.putExtra("user_email",u.getEmail());
                            startActivity(intent);
                            
                        
                        }else{
                            Toast.makeText(MainActivity.this, "Please check your credentials", Toast.LENGTH_SHORT).show();

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();

                    }
                });



            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (firebaseAuth.getCurrentUser() != null) {
            startActivity(new Intent(MainActivity.this,HomePage.class));
        }
    }
}