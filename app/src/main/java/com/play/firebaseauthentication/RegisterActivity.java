package com.play.firebaseauthentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import Model.User;

public class RegisterActivity extends AppCompatActivity {

    private TextInputLayout firstNameInput,lastNameInput,emailInput,passwordInput;
    private FirebaseAuth mAuth;
    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        firstNameInput = (TextInputLayout) findViewById(R.id.textInputLayout);
        lastNameInput = (TextInputLayout) findViewById(R.id.textInputLayout2);

        emailInput = (TextInputLayout) findViewById(R.id.email_layout);
        passwordInput = findViewById(R.id.password_layout);


        Button signUpBtn = (Button)findViewById(R.id.signUpBtn);


        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String firstName = Objects.requireNonNull(firstNameInput.getEditText()).getText().toString();

                String lastName = Objects.requireNonNull(lastNameInput.getEditText()).getText().toString();
                String email = Objects.requireNonNull(emailInput.getEditText()).getText().toString();

                String password = Objects.requireNonNull(passwordInput.getEditText()).getText().toString();


                mAuth.createUserWithEmailAndPassword(email,password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){

                                    Toast.makeText(RegisterActivity.this, "Succesfully created", Toast.LENGTH_SHORT).show();
                                    FirebaseUser u = mAuth.getCurrentUser();

                                    DatabaseReference myRef = firebaseDatabase.getReference();
                                    assert u != null;
                                    User user = new User( firstName,lastName,u.getEmail());

                                    myRef.child("users").child(u.getUid()).setValue(user);

                                    Intent intent  = new Intent(RegisterActivity.this, HomePage.class);

                                    intent.putExtra("user_email",u.getEmail());
                                    startActivity(intent);




                                }else{
                                    Toast.makeText(RegisterActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }




                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(RegisterActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        });






    }
}