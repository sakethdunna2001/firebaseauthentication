package com.play.firebaseauthentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import Model.User;

public class HomePage extends AppCompatActivity {


    private  FirebaseAuth mAuth;
    private  String email;

    private String firstName;
    private String lastName;

    private EditText editFirstName;
    private EditText editLastName;

    private DatabaseReference ref;
    private FirebaseDatabase firebaseDatabase;
    private Button updateBtn;


    private static final String TAG = ".HomePage";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);


        Button logoutBtn = (Button)findViewById(R.id.logout_btn);
        updateBtn = (Button) findViewById(R.id.update_btn);
         mAuth = FirebaseAuth.getInstance();

         editFirstName = (EditText)findViewById(R.id.edit_first_name);

         editLastName = (EditText)findViewById(R.id.edit_last_name);

        String uid = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        Log.d(TAG, "onCreate: " + uid);


        firebaseDatabase = FirebaseDatabase.getInstance();

         ref = firebaseDatabase.getReference("users/" + uid);



        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {



                User user  = snapshot.getValue(User.class);






                    firstName = user.getFirstName();
                    lastName = user.getLastName();


                if(mAuth.getCurrentUser() != null){

                    email = mAuth.getCurrentUser().getEmail();
                    TextView emailText = (TextView) findViewById(R.id.textView2);
                    TextView nameText = (TextView)findViewById(R.id.welcome_text);

                    String welcomeMsg = "Welcome Back ".concat(firstName).concat(lastName);
                    nameText.setText(welcomeMsg);

                    emailText.setText(email);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
//                Toast.makeText(HomePage.this, error.toString(), Toast.LENGTH_SHORT).show();


                // Do Nothing
            }
        });






         logoutBtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 mAuth.signOut();
                 startActivity(new Intent(HomePage.this,MainActivity.class));
             }
         });



         updateBtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {


                 String firstName = editFirstName.getText().toString();
                 String lastName = editLastName.getText().toString();
                 updateUser(firstName , lastName);



             }
         });

    }



    private void updateUser(String firstName, String lastName){

        User mUser = new User(firstName,lastName, Objects.requireNonNull(mAuth.getCurrentUser()).getEmail());

        ref.setValue(mUser);

    }

    public void sampleMethod(){

        System.out.println("hlo");

        //this is the code that i want to push

        // this is the lastest comment

        List<String> myList = new ArrayList<>();
        myList.add("Saketh");

        System.out.println(myList.get(0));

    }






}